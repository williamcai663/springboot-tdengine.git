package com.service;

import com.entity.SysUser;
import com.mapper.mysql.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    public List<SysUser> list() {
        return this.sysUserMapper.list();
    }

    public int add(SysUser sysUser) {
        return this.sysUserMapper.insert(sysUser);
    }


}
