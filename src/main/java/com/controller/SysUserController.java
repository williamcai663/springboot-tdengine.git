package com.controller;

import com.entity.SysUser;
import com.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sysuser")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @GetMapping("/list")
    public List<SysUser> list() {
        return this.sysUserService.list();
    }

    @PostMapping
    public int add(@RequestBody SysUser sysUser) {
        return this.sysUserService.add(sysUser);
    }

}

