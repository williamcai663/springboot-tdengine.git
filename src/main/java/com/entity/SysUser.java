package com.entity;

import lombok.Data;

@Data
public class SysUser {


    private Long id;

    private String loginName;

    private String nickName;
}
