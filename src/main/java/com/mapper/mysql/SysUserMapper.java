package com.mapper.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> list();
}
